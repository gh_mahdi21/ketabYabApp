/* @flow */
import React,{Component } from 'react';
import {
    ScrollView,
    Text,
    TextInput,
    View,
    Button,
    StyleSheet,
    Image,
    TouchableOpacity,
    Alert,VirtualizedList,TouchableHighlight

} from 'react-native';
import HeaderButton from './components/HeaderButton';

var books=[]

class DataSource {
  getElementAtIndex (index) {
      return { key: index ,name:books[index].name,author:books[index].author,description:books[index].description,image:books[index].image}
  }
}
const d = new DataSource;
function getItem (data, index) {
  return data.getElementAtIndex(index)
}
export default class Home extends Component {

  constructor(props){
     super(props)
     this.state={
       refresh:0,
     }
   }
   componentWillMount(){
     fetch('http://mahdighorbani21.pythonanywhere.com/book/', {
       method: 'GET',
       headers: {
         Accept: 'application/json',
         'Content-Type': 'application/json',
       },
     }).then(response => {
       console.log(response)
       console.log(response._bodyInit)
       books=JSON.parse(response._bodyInit).books
       console.log(books)
       console.log(books.length)
       console.log(books[0])
       this.setState({refresh:1})
     })
   }
   getItemCount (data) {
     return books.length;
   }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <HeaderButton onPress={() => navigate('DrawerOpen')} />
        {/* <TextInput placeholder='جستجو' style={styles.search}/> */}
        <Text style={styles.wellcome}>به اپلیکیشن کتاب یاب خوش‌آمدید</Text>
        {!books.length
          ?<View></View>:
          <VirtualizedList
            style={{width:'100%',marginTop:'5%', backgroundColor:'white' }}
            data={d}
            getItemCount={this.getItemCount}
            getItem={getItem}
            keyExtractor={(item, index) => {
              return item.key
            }}
            renderItem={({ item, index }) => {
              return (
                <TouchableHighlight onPress={() => {
                  navigate('Detail', { book:item});
                }}>
                  <View style={styles.listItem}>
                    <View style={styles.listInfo}>
                      <Text style={styles.titleLabel}>کتاب {item.name} اثر {item.author}</Text>
                      <Text style={styles.memberLabel}>{item.description}</Text>
                    </View>
                    <View style={styles.listIcon}>
                      { (!item.image) ?
                        <Image
                          resizeMode="contain"
                          style={styles.channelIcon}
                          source={require("./images/book.jpg")}
                        /> :
                        <Image
                          resizeMode="contain"
                          style={styles.channelIcon}
                          source={{uri:'http://mahdighorbani21.pythonanywhere.com/static/uploads/'+item.image}}
                        />
                      }
                    </View>
                  </View>
                </TouchableHighlight>
              )
            }}
          />
        }
      </View>

    )
  }
}
const styles = StyleSheet.create({
  container: {
    direction:'ltr',
    flex: 1,
    backgroundColor: '#FFF',
    alignItems: 'flex-end',
  },
  search:{
    textAlign:'center',
    top:15,
    right:'5%',
    marginTop:10,
    width:'80%',
    height:'6%',
    borderWidth:1,
    borderRadius: 4,
    borderColor:'#E2E2E2',
  },
  wellcome:{
    textAlign:'center',
    top:20,
    right:'9%',
    marginTop:10,
    width:'80%',
    height:'6%',
    fontSize:15,
  },

  listItem: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#f7f8fc',
    borderBottomWidth: 0.5,
    borderColor: '#D0DBE4',
    padding: 5
  },
  listIcon: {
    justifyContent: 'flex-end',
    paddingLeft: 10,
    paddingRight: 15,
  },
  channelIcon: {
    borderRadius:5,
    width: 110,
    height: 110
  },
  listInfo: {
    flex: 1,
    alignItems: 'flex-end',
  },
  titleLabel: {
    fontSize: 15,
    fontWeight: '600',
    color: '#60768b',
  },
  memberLabel: {
    marginTop:10,
    fontSize: 14,
    fontWeight: '400',
    color: '#abb8c4',
  }
});
