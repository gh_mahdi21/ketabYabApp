/* @flow */
import React,{Component } from 'react';
import {
    ScrollView,
    Text,
    TextInput,
    View,
    Button,
    StyleSheet,
    Image,
    TouchableOpacity,
    Alert,KeyboardAvoidingView
} from 'react-native';

export default class Login extends Component {
  constructor(props){
     super(props)
     this.state = {
       name:'',
       email:'',
       tell: '',
       address:'',
       password: '',
     }
   }
  render() {
    return (
        <View style={{flex:1,alignItems:'center',justifyContent:'center', backgroundColor: '#FFFCFF' }}>
          <KeyboardAvoidingView style={[styles.container]}>
              <Image source={require("./images/logo.png")} style={styles.image}/>
              <TextInput underlineColorAndroid='rgba(0,0,0,0)' placeholder='نام و نام خانوادگی' style={styles.input}
                returnKeyLabel = {"next"}
                onChangeText={(text) => this.setState({name:text})}
              />

              <TextInput underlineColorAndroid='rgba(0,0,0,0)' placeholder='ایمیل' style={styles.input}
                returnKeyLabel = {"next"}
                onChangeText={(text) => this.setState({email:text})}
              />
              <TextInput underlineColorAndroid='rgba(0,0,0,0)' placeholder='شماره تلفن' style={styles.input}
                returnKeyLabel = {"next"}
                onChangeText={(text) => this.setState({tell:text})}
              />
              <TextInput underlineColorAndroid='rgba(0,0,0,0)' placeholder='آدرس' style={styles.input}
                returnKeyLabel = {"next"}
                onChangeText={(text) => this.setState({address:text})}
                secureTextEntry = {true}
              />
              <TextInput underlineColorAndroid='rgba(0,0,0,0)' placeholder='رمز عبور' style={styles.input}
                returnKeyLabel = {"next"}
                secureTextEntry = {true}
                onChangeText={(text) => this.setState({password:text})}
              />
              <View style={{margin:7}} ></View>
              <TouchableOpacity
                style={styles.enter_buttom}
                onPress = {()=>{
                  fetch('http://mahdighorbani21.pythonanywhere.com/login/', {
                    method: 'POST',
                    headers: {
                      Accept: 'application/json',
                      'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                      name:this.state.name,
                      email:this.state.email,
                      tell: this.state.tell,
                      address:this.state.address,
                      password: this.state.password,
                    }),
                  }).then(response => {
                    console.log(JSON.parse(response._bodyInit).response)
                  })
                }} >
                  <Text style={styles.row_text}>
                    {'ثبت نام'}
                  </Text>
              </TouchableOpacity>
            </KeyboardAvoidingView>
        </View>
    )
    }
}
var styles = StyleSheet.create({
  container: {
    width:300,
    height:450,
    alignItems:'center',
    justifyContent:'center',
  borderWidth:1,
  borderColor:'#E2E2E2',
  backgroundColor:'#fff',
  borderRadius:6,

  },
  input:{
    textAlign:'center',
    marginTop:10,
    width:'90%',
    height:'9%',
    borderWidth:1,
    borderRadius: 4,
    borderColor:'#E2E2E2',
  },
  enter_buttom: {
  backgroundColor: 'rgb(0,126,195)',
  height:'9%',
  width:'90%',
  justifyContent:'center',
  borderColor: '#E2E2E2',
  borderWidth: 1,
  borderRadius: 4,
  padding: 5,
},
guest_buttom: {
backgroundColor: 'rgb(0,174,195)',
height:'9%',
width:'90%',
justifyContent:'center',
borderColor: '#E2E2E2',
borderWidth: 1,
borderRadius: 4,
padding: 5,
},
row_text: {
  flex: 1,
  fontSize: 18,
  alignSelf: 'center',
  color:'#FFF',
  justifyContent:'center',
},
image:{
    height: '30%',
    resizeMode: 'contain'
}
});
