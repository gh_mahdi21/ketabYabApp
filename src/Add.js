/* @flow */
import React,{Component } from 'react';
import {
    ScrollView,
    Text,
    TextInput,
    View,
    Button,
    StyleSheet,
    Image,
    TouchableOpacity,
    Alert,KeyboardAvoidingView
} from 'react-native';

export default class Login extends Component {
  constructor(props){
     super(props)
     this.state={
       name : '',
       author : '',
       genre : '',
       age : '',
       pubyear : '',
       dimension : '',
       description : '',
       summary : '',
       tags : '',
       refresh:'false'
      //  image : models.ImageField(upload_to=user_directory_path,null=True)
     }
   }
  render() {
        return (
          <ScrollView ref="scrollView">
            <View style={{flex:1,alignItems:'center',justifyContent:'center', backgroundColor: '#FFFCFF' }}>
              <View style={[styles.container]}>
                <Text style={{fontSize:20,fontWeight:'bold'}}>ثبت کتاب جدید</Text>
                <TextInput underlineColorAndroid='rgba(0,0,0,0)' placeholder='نام' style={styles.input}
                  returnKeyLabel = {"next"}
                  onChangeText={(text) => this.setState({name:text})}
                  ref={input => { this.textInput = input }}
                />
                <TextInput underlineColorAndroid='rgba(0,0,0,0)' placeholder='نویسنده' style={styles.input}
                  returnKeyLabel = {"next"}
                  onChangeText={(text) => this.setState({author:text})}
                  ref={input => { this.textInput2 = input }}
                />
                <TextInput underlineColorAndroid='rgba(0,0,0,0)' placeholder='ژانر' style={styles.input}
                  returnKeyLabel = {"next"}
                  onChangeText={(text) => this.setState({genre:text})}
                  ref={input => { this.textInput3 = input }}
                />
                <TextInput underlineColorAndroid='rgba(0,0,0,0)' placeholder='گروه سنی' style={styles.input}
                  returnKeyLabel = {"next"}
                  onChangeText={(text) => this.setState({age:text})}
                  ref={input => { this.textInput4 = input }}
                />
                <TextInput underlineColorAndroid='rgba(0,0,0,0)' placeholder='سال انتشار' style={styles.input}
                  returnKeyLabel = {"next"}
                  onChangeText={(text) => this.setState({pubyear:text})}
                  ref={input => { this.textInput5 = input }}
                />
                <TextInput underlineColorAndroid='rgba(0,0,0,0)' placeholder='ابعاد' style={styles.input}
                  returnKeyLabel = {"next"}
                  onChangeText={(text) => this.setState({dimension:text})}
                  ref={input => { this.textInput6 = input }}
                />
                <TextInput underlineColorAndroid='rgba(0,0,0,0)' placeholder='توضیحات' style={styles.bigInput}
                  returnKeyLabel = {"next"}
                  multiline = {true} numberOfLines = {4}
                  onChangeText={(text) => this.setState({description:text})}
                  ref={input => { this.textInput7 = input }}
                />
                <TextInput underlineColorAndroid='rgba(0,0,0,0)' placeholder='خلاصه' style={styles.bigInput}
                  returnKeyLabel = {"next"}
                  multiline = {true} numberOfLines = {4}
                  onChangeText={(text) => this.setState({summary:text})}
                  ref={input => { this.textInput8 = input }}
                />
                <TextInput underlineColorAndroid='rgba(0,0,0,0)' placeholder='کلمات کلیدی' style={styles.input}
                  returnKeyLabel = {"next"}
                  onChangeText={(text) => this.setState({tags:text})}
                  ref={input => { this.textInput9 = input }}
                />
                <View style={{margin:7}} ></View>
                <TouchableOpacity
                  style={styles.enter_buttom}
                  onPress = {()=>{
                    fetch('http://mahdighorbani21.pythonanywhere.com/book/add/', {
                      method: 'POST',
                      headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                      },
                      body: JSON.stringify({
                        name : this.state.name,
                        author : this.state.author,
                        genre : this.state.genre,
                        age : this.state.age,
                        pubyear : this.state.pubyear,
                        dimension : this.state.dimension,
                        description : this.state.description,
                        summary : this.state.summary,
                        tags : this.state.tags,
                      }),
                    }).then(response => {
                      if(JSON.parse(response._bodyInit).response=='ok'){
                        Alert.alert('کتاب شما با موفقیت ثبت گردید')
                        this.setState({
                          name : '',
                          author : '',
                          genre : '',
                          age : '',
                          pubyear : '',
                          dimension : '',
                          description : '',
                          summary : '',
                          tags : '',
                        })
                        this.textInput.clear()
                        this.textInput2.clear()
                        this.textInput3.clear()
                        this.textInput4.clear()
                        this.textInput5.clear()
                        this.textInput6.clear()
                        this.textInput7.clear()
                        this.textInput8.clear()
                        this.textInput9.clear()
                        this.refs.scrollView.scrollTo(0);
                      }else{
                        Alert.alert('ثبت نام شما با مشکل مواجه شد لفطا دوباره تلاش کنید')
                      }
                    })
                  }} >
                    <Text style={styles.row_text}>
                      {'ثبت'}
                    </Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>

        )
    }
}

var styles = StyleSheet.create({
  container: {
    marginVertical:30,
    width:300,
    height:750,
    alignItems:'center',
    justifyContent:'center',
  borderWidth:1,
  borderColor:'#E2E2E2',
  backgroundColor:'#fff',
  borderRadius:6,

  },
  input:{
    textAlign:'center',
    marginTop:10,
    width:'90%',
    height:'6%',
    borderWidth:1,
    borderRadius: 4,
    borderColor:'#E2E2E2',
  },
  bigInput:{
    textAlign:'center',
    marginTop:10,
    width:'90%',
    height:'12%',
    borderWidth:1,
    borderRadius: 4,
    borderColor:'#E2E2E2',
  },
  enter_buttom: {
  backgroundColor: 'rgb(0,126,195)',
  height:'6%',
  width:'90%',
  justifyContent:'center',
  borderColor: '#E2E2E2',
  borderWidth: 1,
  borderRadius: 4,
  padding: 5,
},
row_text: {
  flex: 1,
  fontSize: 18,
  alignSelf: 'center',
  color:'#FFF',
  justifyContent:'center',
}
});
