 import React from 'react';
 import {
   DrawerNavigator,
   StackNavigator,
 } from 'react-navigation';
 import Home from './src/Home';
 import Login from './src/Login';
 import Drawer from './src/Drawer';
 import Chat from './src/Chat';
 import Change from './src/Change';
 import Add from './src/Add';
 import Edit from './src/Edit';
 import SignUp from './src/SignUp';
 import ResetPassword from './src/ResetPassword';
 import Invitation from './src/Invitation';

 const TabNavigation = StackNavigator({
   Login: {
     screen: Login,
     navigationOptions: {
       header: null,
       headerBackTitle: 'ورود',
     },
   },
   Change: {
     screen: Change,
     navigationOptions:{
       title: 'ویرایش یا حذف کتاب',
       headerBackTitle: 'منوی اصلی',
     },
   },
   Add: {
     screen: Add,
     navigationOptions: {
       title: 'اضافه کردن کتاب',
       headerBackTitle: 'منوی اصلی',
     },
   },
   Edit: {
     screen: Edit,
     navigationOptions: {
       title: 'ویرایش',
       headerBackTitle: 'منوی اصلی',
     },
   },
   ResetPassword:{
     screen: ResetPassword,
     navigationOptions: {
       title: '               ثبت نام',
       headerBackTitle: '',
     },
   },
   SignUp:{
     screen: SignUp,
     navigationOptions: {
       title: '               ثبت نام',
       headerBackTitle: '',
     },
   },
   Home: {
     screen: Home,
     navigationOptions: {
       header: null,
       headerBackTitle: 'منو',
     },
   },
   Invitation: {
     screen: Invitation,
     navigationOptions: {
       title: 'دعوت',
       headerBackTitle: 'منوی اصلی',
     },
   },
   Chat: {
     screen: Chat,
     navigationOptions: ({ navigation }) => ({
       title: `${navigation.state.params.user}`,
     }),
   },
 });
 const TabsWithDrawerNavigation = DrawerNavigator({
   Tabs: {
     screen: TabNavigation,
   }
 }, {
   contentComponent: props => <Drawer {...props} />
 });

 export default StackNavigator({
   TabsWithDrawer: {
     screen: TabsWithDrawerNavigation,
   },
 }, {
   headerMode: 'none',
 });
