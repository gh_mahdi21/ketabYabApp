/* @flow */
import React,{Component } from 'react';
import {
    ScrollView,
    Text,
    TextInput,
    View,
    Button,
    StyleSheet,
    Image,
    TouchableOpacity,
    Alert
} from 'react-native';

export default class Login extends Component {
  constructor(props){
     super(props)
   }
   render() {
     const { navigate } = this.props.navigation;
     return (
       <View style={styles.container}>
         <Text style={styles.header}>
         </Text>
         <TouchableOpacity
           onPress={() => {
             navigate('Add')
           }}
         >
           <Text style={[styles.links,{color:'rgb(0,126,195)'}]}>
             {"اضافه کردن کتاب"}
           </Text>
         </TouchableOpacity>

         <TouchableOpacity
           onPress={() => {
             navigate('Change')
           }}
         >
           <Text style={[styles.links,{color:'rgb(0,126,195)'}]}>
             {"ویرایش یا حذف کتاب"}
           </Text>
         </TouchableOpacity>
         <TouchableOpacity
           onPress={() => {
             navigate('Invitation')
           }}
         >
           <Text style={[styles.links,{color:'rgb(0,126,195)'}]}>
             {"دعوت دوستان"}
           </Text>
         </TouchableOpacity>
         <TouchableOpacity
           onPress={() => {
             fetch('http://mahdighorbani21.pythonanywhere.com/login/', {
               method: 'POST',
               headers: {
                 Accept: 'application/json',
                 'Content-Type': 'application/json',
               },
               body: JSON.stringify({
                 logout: true,
               }),
             })
             navigate('Login')
           }}
         >
           <Text style={[styles.links,{color:'rgb(236,96,138)'}]}>
             {"خروج"}
           </Text>
         </TouchableOpacity>
       </View>
     );
   }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    alignItems: 'center',
    justifyContent: 'center',
  },
  links:{
    marginVertical:15,
    fontSize:20,
    color:'rgb(0,126,195)',
  },
  header: {
    fontSize: 20,
    marginVertical: 20,
  },
});
