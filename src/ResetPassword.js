/* @flow */
import React,{Component } from 'react';
import {
    ScrollView,
    Text,
    TextInput,
    View,
    Button,
    StyleSheet,
    Image,
    TouchableOpacity,
    Alert,KeyboardAvoidingView
} from 'react-native';

export default class Login extends Component {
  constructor(props){
     super(props)
     this.state = {
       username: '',
       password: '',
       email:'',
     }
   }
  render() {
    return (
        <View style={{flex:1,alignItems:'center',justifyContent:'center', backgroundColor: '#FFFCFF' }}>

          <KeyboardAvoidingView style={[styles.container]}>
              <Image source={require("./images/logo.png")} style={styles.image}/>
              <TextInput underlineColorAndroid='rgba(0,0,0,0)' placeholder='ایمیل' style={styles.input}
                returnKeyLabel = {"next"}
                onChangeText={(text) => this.setState({email:text})}
              />
              <View style={{margin:7}} ></View>
              <TouchableOpacity
                style={styles.enter_buttom}
                onPress = {()=>{
                  fetch('http://mahdighorbani21.pythonanywhere.com/resetPassword/', {
                    method: 'POST',
                    headers: {
                      Accept: 'application/json',
                      'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                      email:this.state.email,
                    }),
                  }).then(response => {
                    console.log(JSON.parse(response._bodyInit).response)
                  })
                }} >
                  <Text style={styles.row_text}>
                    {'ارسال لینک'}
                  </Text>
              </TouchableOpacity>
            </KeyboardAvoidingView>
        </View>
    )
    }
}
var styles = StyleSheet.create({
  container: {
    width:300,
    height:350,
    alignItems:'center',
    justifyContent:'center',
  borderWidth:1,
  borderColor:'#E2E2E2',
  backgroundColor:'#fff',
  borderRadius:6,

  },
  input:{
    textAlign:'center',
    marginTop:10,
    width:'90%',
    height:'12%',
    borderWidth:1,
    borderRadius: 4,
    borderColor:'#E2E2E2',
  },
  enter_buttom: {
  backgroundColor: 'rgb(0,126,195)',
  height:'12%',
  width:'90%',
  justifyContent:'center',
  borderColor: '#E2E2E2',
  borderWidth: 1,
  borderRadius: 4,
  padding: 5,
},
guest_buttom: {
backgroundColor: 'rgb(0,174,195)',
height:'12%',
width:'90%',
justifyContent:'center',
borderColor: '#E2E2E2',
borderWidth: 1,
borderRadius: 4,
padding: 5,
},
row_text: {
  flex: 1,
  fontSize: 18,
  alignSelf: 'center',
  color:'#FFF',
  justifyContent:'center',
},
image:{
    height: '30%',
    resizeMode: 'contain'
}
});
